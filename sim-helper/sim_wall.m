function F_wall = sim_wall(x_EE,config)

    x_wall = config.sim.params.env.x_wall; 
    K_wall = config.sim.params.env.K_wall; 
    
    if x_EE(1) <= x_wall(1)
        x_EE(1) = x_wall(1);
    end
  
    F_wall = K_wall*(x_wall - x_EE);
    F_wall(2:3) = [0;0];
end