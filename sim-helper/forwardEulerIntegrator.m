function y = forwardEulerIntegrator(u,y0,tStep)

    persistent y_previous
    
    if isempty(y_previous)
        
        y_previous = y0;
    end
    
    % Euler forward integrator
    y = y_previous + tStep*u;
    
    % update previous step
    y_previous = y;
end