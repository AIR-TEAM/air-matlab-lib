% checkNaN checks if among the values of a vector there is one or more NaN,
%          and returns 1. It returns 0 otherwise.
%
function y = checkNaN(u)

   y   = 0;
   tol = 0.01;
   
   if sum(isnan(u)) > tol
   
       y = 1;
   end
end