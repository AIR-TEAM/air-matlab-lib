function config_out = addWaypoint(p,rpy,v,omega,a,omega_dt,t,config_in)

config_out = config_in;   

config_out.trajectory.position.waypoints = [config_out.trajectory.position.waypoints ...
                                            p];

config_out.trajectory.orientation.waypoints = [config_out.trajectory.orientation.waypoints ...
                                            rpy];

config_out.trajectory.timepoints = [config_out.trajectory.timepoints ...
                                            t]; 

if isempty(v)
    v=[0;0;0];
end
config_out.trajectory.velocity.waypoints = [config_out.trajectory.velocity.waypoints ...
                                            v];

if isempty(omega)
    omega=[0;0;0];
end
config_out.trajectory.angular_velocity.waypoints = [config_out.trajectory.angular_velocity.waypoints ...
                                            omega];


if isempty(a)
    a=[0;0;0];
end
config_out.trajectory.acceleration.waypoints = [config_out.trajectory.acceleration.waypoints ...
                                            a];

if isempty(omega_dt)
    omega_dt=[0;0;0];
end
config_out.trajectory.angular_acceleration.waypoints = [config_out.trajectory.angular_acceleration.waypoints ...
                                            omega_dt];

end