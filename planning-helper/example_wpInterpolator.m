%% example on how to use waypointInterpolator 

%% simulation parameters
dt = 0.01; % corresponds to control frequency

%% Define your Trajectory waypoints and time-stamps
config.trajectory.position.waypoints = [];
config.trajectory.orientation.waypoints = [];
config.trajectory.velocity.waypoints = [];
config.trajectory.angular_velocity.waypoints = [];
config.trajectory.acceleration.waypoints = [];
config.trajectory.angular_acceleration.waypoints = [];
config.trajectory.timepoints = [];

config = addWaypoint([-0.28;0.92;0],[0;0;0],[],[],[],[],0,config);
config = addWaypoint([-0.28;0.92;0],[0;0;0],[],[],[],[],3,config);
config = addWaypoint([-0.28;0.92;0.6],[0;0;0],[],[],[],[],2,config);
config = addWaypoint([1.18;0.92;0.6],[0;0;0],[],[],[],[],3,config);
config = addWaypoint([1.18;0.0;0.6],[0;0;0],[],[],[],[],3,config);
config = addWaypoint([0.0;0.0;0.7],[0;0;0],[],[],[],[],4,config);
config = addWaypoint([-0.28;0.92;0.2],[0;0;0],[],[],[],[],3,config);



%%

absolute_timer = 0;
total_traj_time = sum(config.trajectory.timepoints);
N =  total_traj_time/dt;

wp_idx = 2; % wp_idx always starts from waypoint 2 which 
% is the robot's first waypoint since waypoint 1 should be 
% its initial starting point. 
relative_timer = 0;
current_wp_dur = config.trajectory.timepoints(wp_idx);

position_data = [];
orientation_data = [];

for i=1:N

    if wp_idx < size(config.trajectory.timepoints,2)

        [pdes,vdes,ades,R_des,rpydes,omega,omega_dt] = waypointInterpolator(relative_timer,wp_idx,config);
        absolute_timer = absolute_timer + dt;
        relative_timer = relative_timer + dt;
        if relative_timer > current_wp_dur
                wp_idx = wp_idx + 1;
                relative_timer = 0;
                current_wp_dur = config.trajectory.timepoints(wp_idx);
        end
    else
        absolute_timer = absolute_timer + dt;
        relative_timer = relative_timer + dt;
        [pdes,vdes,ades,R_des,rpydes,omega,omega_dt] = waypointInterpolator(relative_timer,wp_idx,config);
    end

   position_data = [position_data pdes];
   orientation_data = [orientation_data rpydes];
end

t = 0+dt:dt:total_traj_time;
figure
plot(t,position_data(1,:))
title('x')
grid on

figure
plot(t,position_data(2,:))
title('y')
grid on

figure
plot(t,position_data(3,:))
title('z')
grid on


figure
plot(t,rad2deg(orientation_data(1,:)))
title('roll')
grid on

figure
plot(t,rad2deg(orientation_data(2,:)))
title('pitch')
grid on

figure
plot(t,rad2deg(orientation_data(3,:)))
title('yaw')
grid on

