function W_R_e = myUavManip_EE_Fix(W_R_e1)
     
    Re1_e = [0    0.8660    -0.5000;...
            0   -0.5000    -0.8660;...
            -1    0         0];


    W_R_e = W_R_e1*Re1_e;
end