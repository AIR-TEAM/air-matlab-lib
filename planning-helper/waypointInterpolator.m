function [pdes,vdes,ades,R_des,rpydes,omega,omega_dt] = waypointInterpolator(timer,wp_idx,config)

traj_dur = config.trajectory.timepoints(wp_idx);

%% %%%%%%%%%%%%%%%%%%%%%
p0 = config.trajectory.position.waypoints(:,wp_idx-1);
pf = config.trajectory.position.waypoints(:,wp_idx);

o0 = config.trajectory.orientation.waypoints(:,wp_idx-1);
of = config.trajectory.orientation.waypoints(:,wp_idx);
            
v0 = config.trajectory.velocity.waypoints(:,wp_idx-1);
vf = config.trajectory.velocity.waypoints(:,wp_idx);
        
w0 = config.trajectory.angular_velocity.waypoints(:,wp_idx-1);
wf = config.trajectory.angular_velocity.waypoints(:,wp_idx);
        
a0 = config.trajectory.acceleration.waypoints(:,wp_idx-1);
af = config.trajectory.acceleration.waypoints(:,wp_idx);
        
wd0 = config.trajectory.angular_acceleration.waypoints(:,wp_idx-1);
wdf = config.trajectory.angular_acceleration.waypoints(:,wp_idx);
       
%% %%%%%%%%%%%%%%%%%%%%%%%%%
[px, vx, ax] = QuinticPoly(p0(1), pf(1), v0(1), ...
    vf(1), a0(1), af(1), timer,traj_dur);
[py, vy, ay] = QuinticPoly(p0(2), pf(2), v0(2), ...
        vf(2), a0(2), af(2), timer,traj_dur);
[pz, vz, az] = QuinticPoly(p0(3), pf(3), v0(3), ...
        vf(3), a0(3), af(3), timer,traj_dur);

pdes = [px;py;pz];
vdes = [vx;vy;vz]; 
ades = [ax;ay;az];    
    
[ox, wx, wdx] = QuinticPoly(o0(1), of(1), w0(1), ...
        wf(1), wd0(1), wdf(1), timer, traj_dur);
[oy, wy, wdy] = QuinticPoly(o0(2), of(2), w0(2), ...
        wf(2), wd0(2), wdf(2), timer, traj_dur);
[oz, wz, wdz] = QuinticPoly(o0(3), of(3), w0(3), ...
        wf(3), wd0(3), wdf(3), timer, traj_dur);
    
T_ref = [[cos(oy)*cos(oz), -sin(oz), 0];
         [cos(oy)*sin(oz), cos(oz), 0]
         [-sin(oy), 0, 1]];
    
Tdot_ref = [[-wy*sin(oy)*cos(oz)- wz*cos(oy)*sin(oz), -wz*cos(oz), 0];
         [-wy*sin(oy)*sin(oz) + wz*cos(oy)*cos(oz), -wz*sin(oz), 0]
         [-wy*cos(oy), 0, 0]];
    
R_des = Rz(oz)*Ry(oy)*Rx(ox);
    
rpydes = [ox;oy;oz];
    
omega = R_des'*T_ref*[wx;wy;wz];
    
omega_dt = R_des'*(T_ref*[wdx;wdy;wdz] + Tdot_ref*[wx;wy;wz]) ;

end
