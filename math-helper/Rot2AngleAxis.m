function [u,theta] = Rot2AngleAxis(R)
 nom  = sqrt((R(1,2)-R(2,1))^2 + (R(1,3)-R(3,1))^2 ...
        + (R(2,3)-R(3,2))^2); % can be positive or negative
denom = R(1,1) + R(2,2) + R(3,3)-1;

theta = atan2(nom,denom);

if(nom == 0)
    if (theta == 0)
        u = [-1;-1;-1]; %undefined
    else
        u = [sqrt((R(1,1)+1)/2);sqrt((R(2,2)+1)/2);sqrt((R(3,3)+1)/2)];
    end
else
    u = (1/(2*sin(theta)))*...
        [R(3,2)-R(2,3);R(1,3)-R(3,1);R(2,1)-R(1,2)];
end
 
 
end