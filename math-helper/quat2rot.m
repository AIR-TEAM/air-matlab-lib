function R = quat2rot(q)

R=eye(3);

eta=q(1);
ex=q(2);
ey=q(3);
ez=q(4);

R=[2*(eta^2+ex^2)-1 2*(ex*ey-eta*ez) 2*(ex*ez+eta*ey);
2*(ex*ey+eta*ez) 2*(eta^2+ey^2)-1 2*(ey*ez-eta*ex);
2*(ex*ez-eta*ey) 2*(ey*ez+eta*ex) 2*(eta^2+ez^2)-1];

end