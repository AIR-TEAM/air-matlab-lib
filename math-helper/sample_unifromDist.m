function delta = sample_unifromDist(range_mat)
%SAMPLE_UNIFROMDIST: sample from the uniform distribution given by the
%ranges of the parameters of the system
datalen = size(range_mat,1);
max = zeros(datalen,1);
min = zeros(datalen,1);
for i=1:datalen
    max(i) = range_mat(i,i);
    min(i) = -range_mat(i,i);
end
delta_0 = rand(datalen,1); %random num from 0 to 1
delta = min + (max - min).*delta_0;
end
