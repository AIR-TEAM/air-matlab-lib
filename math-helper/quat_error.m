function e_quat = quat_error(qa,qb)

    q1 = qa(1);
    x1 = qa(2);
    y1 = qa(3);
    z1 = qa(4);
    
    q2 = qb(1);
    x2 = qb(2);
    y2 = qb(3);
    z2 = qb(4);

    dq =  q1*q2 + x1*x2 + y1*y2 + z1*z2; 
    dx =  q1*x2 - x1*q2 + y1*z2 - z1*y2;
    dy =  q1*y2 - y1*q2 - x1*z2 + z1*x2;
    dz =  q1*z2 - q2*z1 + x1*y2 - y1*x2;

    e_quat = [dq;dx;dy;dz];

end