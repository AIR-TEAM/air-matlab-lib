function [roll,pitch,yaw] = Rotation2Rpy(R)

roll = atan2(R(3,2),R(3,3));
pitch = atan2(-R(3,1),sqrt(R(3,2)^2+R(3,3)^2));
yaw = atan2(R(2,1),R(1,1));

end