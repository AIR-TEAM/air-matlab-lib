function [angle_xy,axis_xy,axis_z,error_xy,error_z] = S2error(R_des,Rr)

    Re = R_des'*Rr;
    %Rth axis angle 
    e3 = [0;0;1];
    Re_33 = e3'*Re*e3;
    if Re_33 <= 1 && Re_33 >= -1
        angle_xy = acos(e3'*Re*e3);
    else
        angle_xy = 0;
    end

    if angle_xy == 0 
        axis_xy = zeros(3,1);
        error_xy = zeros(3,1);
        [axis_z,error_z] = Rot2AngleAxis(Re);
    else
        axis_xy = cross(Re'*e3,e3)/norm(cross(Re'*e3,e3));
        if angle_xy <= pi/2
            error_xy = sin(angle_xy)*axis_xy;
        else
            error_xy = axis_xy;
        end
        error_xy = sin(angle_xy)*axis_xy;
        Rth = axisAngle2Rot(axis_xy,angle_xy);
        Ryaw = Re*inv(Rth);
        [axis_z,error_z] = Rot2AngleAxis(Ryaw);
    end

end

