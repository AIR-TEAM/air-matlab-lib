function quat = ro2quat(Ree)
    m00 = Ree(1,1);
    m01 = Ree(1,2);
    m02 = Ree(1,3);

    m10 = Ree(2,1);
    m11 = Ree(2,2);
    m12 = Ree(2,3);

    m20 = Ree(3,1);
    m21 = Ree(3,2);
    m22 = Ree(3,3);

    temp_expr1 = 1 + m00 + m11 + m22;
    temp_expr2 = temp_expr1^(1/2);
    qw=  temp_expr2/2;
    qx = (m21 - m12)/( 4 *qw);
    qy = (m02 - m20)/( 4 *qw);
    qz = (m10 - m01)/( 4 *qw);
    quat = [qw;qx;qy;qz];
end