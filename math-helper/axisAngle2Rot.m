function Rot = axisAngle2Rot(axis,theta)

rx = axis(1);
ry = axis(2);
rz = axis(3);

rx2 = rx^2;
ry2 = ry^2;
rz2 = rz^2;
c = cos(theta);
I_c = 1 - c;
s = sin(theta);

Rot = [ (rx2*I_c)+c  (rx*ry*I_c)-rz*s (rx*rz*I_c)+ry*s;...
        (rx*ry*I_c)+rz*s  (ry2*I_c)+c (ry*rz*I_c)-rx*s;...
        (rx*rz*I_c)-ry*s  (ry*rz*I_c)+rx*s (rz2*I_c)+c];

end

