function [position, velocity, acceleration] = QuinticPoly(initialPos, finalPos, initialVel, finalVel, initialAcc, finalAcc, currentTime, totalTime)
    if currentTime >= totalTime
        % Set the current time to the total time value
        currentTime = totalTime;
    end
    
    % Compute the coefficients of the quintic polynomial
    timeVec = [1, currentTime, currentTime^2, currentTime^3, currentTime^4, currentTime^5];
    A = [1, 0, 0, 0, 0, 0;
         0, 1, 0, 0, 0, 0;
         0, 0, 2, 0, 0, 0;
         1, totalTime, totalTime^2, totalTime^3, totalTime^4, totalTime^5;
         0, 1, 2*totalTime, 3*totalTime^2, 4*totalTime^3, 5*totalTime^4;
         0, 0, 2, 6*totalTime, 12*totalTime^2, 20*totalTime^3];
    b = [initialPos; initialVel; initialAcc; finalPos; finalVel; finalAcc];
    coeff = A \ b;
    
    % Compute the current position, velocity, and acceleration
    position = coeff' * timeVec';
    velocity = coeff(2:end)' * [1, 2*currentTime, 3*currentTime^2, 4*currentTime^3, 5*currentTime^4]';
    acceleration = coeff(3:end)' * [2, 6*currentTime, 12*currentTime^2, 20*currentTime^3]';
end

