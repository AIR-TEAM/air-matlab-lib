function [Hessian, gradient, upperBounds, lowerBounds,  EqualityConstrMatrix, ...
          biasVectorEqConstr, InequalityConstrMatrix, ubiasVectorIneqConstr, lbiasVectorIneqConstr] = ...
             cntrl_INDI_QP(aref,M,R_v,af,uf,config)


    %% initialization
    weights_task1 = config.cntrl_QP.weights.uav_translation_task;
    weights_task2 = config.cntrl_QP.weights.uav_orientation_task;
    umax = config.sat.maxThrust;
    umin = config.sat.minThrust;
    R_G_v = [R_v zeros(3);zeros(3) eye(3)];
    G_v = R_G_v*config.cntrl_QP.params.uav_wrenchMap;


    W = [diag([weights_task1;weights_task1;weights_task1]) zeros(3,3); ...
        zeros(3,3) diag([weights_task2;weights_task2;weights_task2])];

    I = M\G_v;
    b = af - M\(G_v*uf) - aref;

    W_prime = I'*W*I;
     
    %% QP variables definition
   
    
    % Hessian matrix
    Hessian  = W_prime;
    
    % enforce Hessian matrix symmetry
    Hessian  = 0.5*(transpose(Hessian) + Hessian);
    
    % gradient       
    gradient = (b'*W*I)';
          
    % upper and lower bounds
    upperBounds = umax*ones(6,1);
    lowerBounds = umin*ones(6,1);

    % equality constraint

    EqualityConstrMatrix = zeros(1,6);
    biasVectorEqConstr = 0;


    InequalityConstrMatrix = I;
    
    lbiasVectorIneqConstr = 0;
                         
    ubiasVectorIneqConstr = 0;




end