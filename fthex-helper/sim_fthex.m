function [p_v_ddt,w_v_dt] = sim_fthex(prop_speed,FText,residual_W,Rr,omega_v,config)
%% General simulation for the fthex that considers the presence of residual 
%%dynamics and external forces

%% initializations

M_v = eye(3)*config.sim.params.uav_mass; % uav mass
I_v = config.sim.params.uav_inertia;%3x3 diag. uav Inertia mat.
G_v = config.sim.params.uav_wrenchMap;
R_G_v = [Rr zeros(3);zeros(3) eye(3)];
g = 9.81;

%% Control wrench computation
cntrl_W = R_G_v*G_v*prop_speed;

%% forward dynamics
% translation dynamics M_v*p_v_ddt + M_v*g*e3 ...
% + residual_W(1:3) = cntrl_W(1:3) + f_ext
p_v_ddt = M_v\(-M_v*g*[0;0;1] - residual_W(1:3) + cntrl_W(1:3) + ...
    FText(1:3));

% orientation dynamics Jw_dt + w x Jw + res_W = cntrl_W + tau_ext
I_omega_v = I_v*omega_v;
w_v_dt = I_v\(-cross(omega_v,I_omega_v) - residual_W(4:6) + cntrl_W(4:6)  ...
    + FText(4:6));

end