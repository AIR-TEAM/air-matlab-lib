function [prop_speed_sq,tau_m] = uavmanip_ipMap(W_cntrl,R_v,config)

G_v = config.cntrl_FL.params.uav_wrenchMap;
R_G_v = [R_v zeros(3);zeros(3) R_v];

w_G_v = R_G_v*G_v;

ipMap = [w_G_v zeros(6,3);...
        zeros(3,6) eye(3)];

ip = ipMap\W_cntrl;

prop_speed_sq = ip(1:6);
tau_m = ip(7:9);

end