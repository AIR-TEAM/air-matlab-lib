function [u_predict,u_update] = wp_KF(u_cmnd,u_meas,Sigma_prev,Sigma_meas,config)
    
    % process model: prop_speed^2 = wrench_map * desired_control_thrust
    % in linear state space form: prop_speed^2 = B*u_cmnd
    % where B, the input matrix =  wrench_map
    % G_v has some uncertainity which should be taken into account 
    % as input noise

    %% Prediction Step
    G_v = config.wp_KF.params.uav_wrenchMap;
    u_predict = G_v*u_cmnd;
    Sigma_predict = G_v*Sigma_prev*G_v';

    %% Update Step
    Kalman_gain = Sigma_predict*inv(Sigma_meas + Sigma_predict);
    u_update = u_predict + Kalman_gain*(u_meas - u_predict);
    Sigma_update = (eye(6) - Kalman_gain)*Sigma_predict;
    
end