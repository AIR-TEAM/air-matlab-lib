function prop_speed_sq = ppcntrl_FL(u_virt,R_v,omega_v,residual_W,FText,ADD_MASS,config)
    %% initializations
    
    if ADD_MASS == 1
        M_v = eye(3)*(config.cntrl_FL.params.uav_mass ...
        + config.cntrl_FL.params.payload_mass); % uav mass
        G_v = config.cntrl_FL.params.uav_wrenchMap_p_c;
    else
        M_v = eye(3)*config.cntrl_FL.params.uav_mass; % uav mass
        G_v = config.cntrl_FL.params.uav_wrenchMap;
    end
        
    I_v = config.cntrl_FL.params.uav_inertia;%3x3 diag. uav Inertia mat.
    I_omega_v = I_v*omega_v;
    g = 9.81;
    Inertia_mat = [M_v zeros(3);zeros(3) I_v];
    bias_forces = [M_v*g*[0;0;1];cross(omega_v,I_omega_v)];
    R_G_v = [R_v zeros(3);zeros(3) eye(3)];
    
    %% feedback linearization
    cntrl_W  = Inertia_mat*u_virt + bias_forces + residual_W - FText;
    prop_speed_sq = (R_G_v*G_v)\cntrl_W;
    
    end
    