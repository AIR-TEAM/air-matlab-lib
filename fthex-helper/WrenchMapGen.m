function W_map = WrenchMapGen(c_t,c_f,al,be,L)

    %% multi-rotor parameters - equidistant assumption - neglect elevation
    numOfProps = 6;
    yaw_1 = 0;
    sign = -1;
    k_i = -1;
    W_map = [];
    b_p_1 = [L;0;0];

    for i=1:numOfProps
        

        %% The propeller pose in frame b
        b_R_pi = Rz(yaw_1)*Rx(sign*al(i))*Ry(be);
        b_z_pi = b_R_pi*[0;0;1];
        b_p_i = Rz(yaw_1)*b_p_1;
        
        %% Drag and Thrust of prop_i expressed in Frame b
        b_m_i = k_i*c_t(i)*(b_z_pi) + c_f(i)*(cross(b_p_i,b_z_pi));
        b_f_i = c_f(i)*(b_z_pi);
        W_map = [W_map [b_f_i;b_m_i]]; 
        
        %% for the next Iteration
        yaw_1 = yaw_1 + pi/3;
        sign = sign*-1;
        k_i = k_i*-1;


    end

end
