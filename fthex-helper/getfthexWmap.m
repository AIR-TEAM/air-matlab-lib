function G_UAV = getfthexWmap(p_c)
%p_c = [alpha;c_f;c_d;lp]

fbthex.arm_length = p_c(4);                                                  %[m] CAD length of the UAV arm                                         %[m] Vertical distance from the motor tilting point to geometric centrer of the propeller
fbthex.alpha = p_c(1);                                            %[rad] alpha tilt angle
fbthex.beta = deg2rad(0.0000);                                             %[rad] beta tilt angle

% 13x4.5 proppeller(note: the value c_f is an upper bound estimate of several proppellers)
fbthex.prop_diam = 13 * (2.54/100);                                        %[m] Propeller diameter 13 inches
fbthex.c_f = p_c(2);%[N*s^2] Parameter of the propeller linking omega_i to f_i                                                     %[N*s^2] Parameter of the propeller linking omega_i to f_i
fbthex.c_d = p_c(3);                                                      %[N.m*s^2] Parameter of the propeller linking omega_i to drag

fbthex.c_f_tau = fbthex.c_d/fbthex.c_f;                                             %[Hz] Frame rate of the Logitech joystick

%Allocation Matrix (in case beta==0) relative to geometric center
a = fbthex.alpha;
l = fbthex.arm_length;
c_f_tau = fbthex.c_f_tau; 
fbthex.allocation = [     0,                      (3^(1/2)*sin(a))/2,                     -(3^(1/2)*sin(a))/2,                         0,                       (3^(1/2)*sin(a))/2,                      -(3^(1/2)*sin(a))/2;
                     sin(a),                               -sin(a)/2,                               -sin(a)/2,                    sin(a),                                -sin(a)/2,                                -sin(a)/2;
                     cos(a),                                  cos(a),                                  cos(a),                    cos(a),                                   cos(a),                                   cos(a);
                          0, (3^(1/2)*(l*cos(a) + c_f_tau*sin(a)))/2, (3^(1/2)*(l*cos(a) + c_f_tau*sin(a)))/2,                         0, -(3^(1/2)*(l*cos(a) + c_f_tau*sin(a)))/2, -(3^(1/2)*(l*cos(a) + c_f_tau*sin(a)))/2;
- l*cos(a) - c_f_tau*sin(a),     - (l*cos(a))/2 - (c_f_tau*sin(a))/2,       (l*cos(a))/2 + (c_f_tau*sin(a))/2, l*cos(a) + c_f_tau*sin(a),        (l*cos(a))/2 + (c_f_tau*sin(a))/2,      - (l*cos(a))/2 - (c_f_tau*sin(a))/2;
  l*sin(a) - c_f_tau*cos(a),               c_f_tau*cos(a) - l*sin(a),               l*sin(a) - c_f_tau*cos(a), c_f_tau*cos(a) - l*sin(a),                l*sin(a) - c_f_tau*cos(a),                c_f_tau*cos(a) - l*sin(a)];


  G_UAV = fbthex.allocation;
  G_UAV = fbthex.c_f * G_UAV;
end

