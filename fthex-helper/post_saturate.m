function u = post_saturate(u,config)
    for i=1:6
        if ~(u(i)<= config.sat.maxThrust && u(i)>= config.sat.minThrust)
               if (u(i) > config.sat.maxThrust)
                   u(i) = config.sat.maxThrust;
               else
                   u(i) = config.sat.minThrust;
               end
        end
           
    end
   
end

