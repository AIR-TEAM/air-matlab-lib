function [Hessian, gradient, upperBounds, lowerBounds,  EqualityConstrMatrix, ...
    biasVectorEqConstr, InequalityConstrMatrix, ubiasVectorIneqConstr, lbiasVectorIneqConstr] = ...
       cbf_QP(u_star,h_x,L_hx,L_hu,config)


%% initialization
W = diag(config.cntrl_QP.weights.input_weights);
umax = config.sat.maxThrust;
umin = config.sat.minThrust;
alpha = config.cbf_QP.alpha;

k_f = 12.413e-4;

%% QP variables definition
% Hessian matrix
W_prime = W;
Hessian  = W_prime;
% enforce Hessian matrix symmetry
Hessian  = 0.5*(transpose(Hessian) + Hessian);

% gradient       
gradient = -W*(u_star);
    
% upper and lower bounds
upperBounds = umax*ones(6,1);
lowerBounds = umin*ones(6,1);

% equality constraint

EqualityConstrMatrix = zeros(1,6);
biasVectorEqConstr = 0;


InequalityConstrMatrix = L_hu;
lbiasVectorIneqConstr = -alpha*h_x - L_hx;                   
ubiasVectorIneqConstr = 1e10;

end