function prop_speed = cntrl_fthex_object(acc_v,FText,residual_W,R_v,omega_v,config)
    %% General simulation for the fthex that considers the presence of residual 
    %%dynamics and external forces
    
    %% initializations
    m_o = config.cntrl_FL.params.payload_mass; % uav mass
    m_v = config.cntrl_FL.params.uav_mass; % uav mass
    m_sys = m_o + m_v;
    I_v = config.cntrl_FL.params.uav_inertia;%3x3 diag. uav Inertia mat.
    G_v = config.cntrl_FL.params.uav_wrenchMap;
    R_G_v = [R_v' zeros(3);zeros(3) eye(3)];
    rc = config.cntrl_FL.params.uav_CoMOff;
    g = 9.81;
    
    Inertia_mat  = [m_sys*eye(3) -m_sys*skewMat(rc);...
                    m_sys*skewMat(rc) I_v-m_sys*skewMat(rc)*skewMat(rc)];
                   

    %% sum of forces
    
    f_tot =  - m_sys*g*R_v'*[0;0;1] - m_sys*skewMat(omega_v)*skewMat(omega_v)*rc + R_v'*residual_W(1:3) ...
        + R_v'*FText(1:3);
    
    tau_tot = - m_sys*g*skewMat(rc)*R_v'*[0;0;1] - skewMat(omega_v)*(I_v - skewMat(rc)*skewMat(rc))*omega_v ...
        + FText(4:6) + residual_W(4:6);

    %% forward dynamics
    b_acc_v = R_G_v*acc_v;
    prop_speed = G_v\(Inertia_mat*b_acc_v - [f_tot;tau_tot]);        

    end