function [u_virt,eI] = PIDPlus(p_v_ref,p_v_dt_ref,p_v_ddt_ref, ...
    R_v_ref,w_v_ref,w_v_dt_ref,...
    p_v,p_v_dt,...
    R_v,w_v,eI_prev,config)
%% initializations
u_virt = zeros(6,1);

Kp = config.cntrl_PDPlus.gains.Kp;
Kv = config.cntrl_PDPlus.gains.Kv;
KR = config.cntrl_PDPlus.gains.KR;
Kw = config.cntrl_PDPlus.gains.Kw;
KI_p = config.cntrl_PDPlus.gains.KI_p;
KI_R = config.cntrl_PDPlus.gains.KI_R;

dt = config.cntrl_PDPlus.param.IntStep;

ep_sat = 0.05;
eIp_sat = 0.05;
ev_sat = 0.05;
eR_sat = deg2rad(5);
eIR_sat = deg2rad(5);
ew_sat = deg2rad(5);

%% error signal computations 

ep =  p_v_ref - p_v;
for i=1:3
    if abs(ep(i)) > ep_sat
        ep(i) = sign(ep(i))*ep_sat;
    end
end


eI_p = eI_prev(1:3) + ep*dt;
for i=1:3
    if abs(eI_p(i)) > eIp_sat
        eI_p(i) = sign(eI_p(i))*eIp_sat;
    end
end

ev = p_v_dt_ref - p_v_dt;
for i=1:3
    if abs(ev(i)) > ev_sat
        ev(i) = sign(ev(i))*ev_sat;
    end
end

eR = vmap(R_v_ref,R_v);
if config.cntrl_PDPlus.intrinsic == 0
eR = R_v*eR;
end

for i=1:3
    if abs(eR(i)) > eR_sat
        eR(i) = sign(eR(i))*eR_sat;
    end
end


ew = w_v_ref - w_v;
for i=1:3
    if abs(ew(i)) > ew_sat
        ew(i) = sign(ew(i))*ew_sat;
    end
end

eI_R = eI_prev(4:6) + (ew+0.8*eR)*dt;
for i=1:3
    if abs(eI_R(i)) > eIR_sat
        eI_R(i) = sign(eI_R(i))*eIR_sat;
    end
end

eI = [eI_p;eI_R];

%% compute u_virt
u_virt(1:3) = p_v_ddt_ref + Kv*ev + Kp*ep + KI_p*eI_p; 
u_virt(4:6) = w_v_dt_ref + Kw*ew + KR*eR + KI_R*eI_R; 


end