function [p_v_ddt,w_v_dt] = sim_CoMOff_fthex(prop_speed,FText,residual_W,R_v,omega_v,config)
    %% General simulation for the fthex that considers the presence of residual 
    %%dynamics and external forces
    
    %% initializations
    
    m_v = eye(3)*config.sim.params.uav_mass; % uav mass
    I_v = config.sim.params.uav_inertia;%3x3 diag. uav Inertia mat.
    G_v = config.sim.params.uav_wrenchMap;
    R_G_v = [R_v zeros(3);zeros(3) eye(3)];
    rc = config.sim.params.uav_CoMOff;
    g = 9.81;
    
    Inertia_mat  = [m_v*eye(3) -m_v*skewMat(rc);...
                    m_v*skewMat(rc) I_v-m_v*skewMat(rc)*skewMat(rc)];


    %% sum of forces
    
    f_tot = G_v(1:3,:)*prop_speed - m_v*g*R_v'*[0;0;1] - m_v*skewMat(omega_v)*skewMat(omega_v)*rc;
    
    tau_tot = G_v(4:6,:)*prop_speed - m_v*g*skewMat(rc)*R_v'*[0;0;1] ... 
            -skewMat(omega_v)*(I_v - skewMat(rc)*skewMat(rc))*omega_v;

    
    %% forward dynamics
    b_acc_v = Inertia_mat\[f_tot;tau_tot];        
    acc_v = R_G_v*b_acc_v;

    % translation dynamics 
    p_v_ddt = acc_v(1:3);
    % orientation dynamics 
    w_v_dt =  acc_v(4:6);
    
    end