function prop_speed_sq = cntrl_stiffness(u_virt,p_v_ddt_ref,p_v_dt_ref,p_v_ref,...
    p_v,p_v_dt,R_v,omega_v,residual_W,FText,config)
%% initializations

M_v = eye(3)*config.cntrl_FL.params.uav_mass; % uav mass
I_v = config.cntrl_FL.params.uav_inertia;%3x3 diag. uav Inertia mat.
I_omega_v = I_v*omega_v;
g = 9.81;
Inertia_mat = [M_v zeros(3);zeros(3) I_v];
bias_forces = [M_v*g*[0;0;1];cross(omega_v,I_omega_v)];
G_v = config.cntrl_FL.params.uav_wrenchMap;
R_G_v = [R_v zeros(3);zeros(3) eye(3)];

%% Impedance computations
M_a = config.cntrl_FL.params.impedance.M_a;
K_a = config.cntrl_FL.params.impedance.K_a;
D_a = config.cntrl_FL.params.impedance.D_a;

imped_motion = p_v_ddt_ref + ...
M_a\(D_a*(p_v_dt_ref - p_v_dt) + K_a*(p_v_ref - p_v));

imped_force = (M_v*inv(M_a) - eye(3));

imped_force = imped_force*FText(1:3);

%% Virtual Ip
u_interact = [imped_motion;u_virt(4:6)];
interact_wrench = [imped_force;-FText(4:6)];

%% feedback linearization
cntrl_W  = Inertia_mat*u_interact + bias_forces + residual_W + interact_wrench;
prop_speed_sq = (R_G_v*G_v)\cntrl_W;

end