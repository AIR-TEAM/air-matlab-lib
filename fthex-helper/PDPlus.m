function u_virt = PDPlus(p_v_ref,p_v_dt_ref,p_v_ddt_ref, ...
                                          R_v_ref,w_v_ref,w_v_dt_ref,...
                                          p_v,p_v_dt,...
                                          R_v,w_v,config)
%% initializations
u_virt = zeros(6,1);

Kp = config.cntrl_PDPlus.gains.Kp;
Kv = config.cntrl_PDPlus.gains.Kv;
KR = config.cntrl_PDPlus.gains.KR;
Kw = config.cntrl_PDPlus.gains.Kw;

ep_sat = 0.1;
ev_sat = 0.05;
eR_sat = deg2rad(10);
ew_sat = deg2rad(5);

%% error signal computations 
ep =  p_v_ref - p_v;
for i=1:3
    if abs(ep(i)) > ep_sat
        ep(i) = sign(ep(i))*ep_sat;
    end
end

ev = p_v_dt_ref - p_v_dt;
for i=1:3
    if abs(ev(i)) > ev_sat
        ev(i) = sign(ev(i))*ev_sat;
    end
end


eR = vmap(R_v_ref,R_v);
if config.cntrl_PDPlus.intrinsic == 0
    eR = R_v*eR;
end

for i=1:3
    if abs(eR(i)) > eR_sat
        eR(i) = sign(eR(i))*eR_sat;
    end
end


ew = w_v_ref - w_v;

for i=1:3
    if abs(ew(i)) > ew_sat
        ew(i) = sign(ew(i))*ew_sat;
    end
end



%% compute u_virt
u_virt(1:3) = p_v_ddt_ref + Kv*ev + Kp*ep; 
u_virt(4:6) = w_v_dt_ref + Kw*ew + KR*eR; 
 


end

