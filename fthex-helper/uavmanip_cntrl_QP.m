function [Hessian, gradient, upperBounds, lowerBounds,  EqualityConstrMatrix, ...
          biasVectorEqConstr, InequalityConstrMatrix, ubiasVectorIneqConstr, lbiasVectorIneqConstr] = ...
             uavTSID(aref,M,h,R_v,config)


    %% initialization
    weights_task1 = config.cntrl_QP.weights.uav_translation_task;
    weights_task2 = config.cntrl_QP.weights.uav_orientation_task;
    umax = config.sat.maxThrust;
    umin = config.sat.minThrust;

    if config.cntrl_QP.intrinsic == 1
        R_G_v = [R_v zeros(3);zeros(3) eye(3)];
        G_v = R_G_v*config.cntrl_QP.params.uav_wrenchMap;
    else
        R_G_v = [R_v zeros(3);zeros(3) R_v];
        G_v = R_G_v*config.cntrl_QP.params.uav_wrenchMap;
        G_u = [G_v zeros(6,3);zeros(3,6) eye(3)];
    end

    W = [diag([weights_task1;weights_task1;weights_task1]) zeros(3,3); ...
        zeros(3,3) diag([weights_task2;weights_task2;weights_task2])];

    I = M\G_v;
    b = -(M\h) - aref;

    W_prime = I'*W*I;
     
    %% QP variables definition
   
    
    % Hessian matrix
    Hessian  = W_prime;
    
    % enforce Hessian matrix symmetry
    Hessian  = 0.5*(transpose(Hessian) + Hessian);
    
    % gradient       
    gradient = (b'*W*I)';
          
    % upper and lower bounds
    upperBounds = umax*ones(6,1);
    lowerBounds = umin*ones(6,1);

    % equality constraint

    EqualityConstrMatrix = [M G_u;J_e zeros(6,9)];
    biasVectorEqConstr = [-h;Jdtnu + aref];


    InequalityConstrMatrix = I;
    
    lbiasVectorIneqConstr = 0;
                         
    ubiasVectorIneqConstr = 0;




end