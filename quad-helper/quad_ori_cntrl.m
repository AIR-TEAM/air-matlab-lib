function m_cmnd = quad_ori_cntrl(fd,yaw_des,R_v_ref_t_1,...
    R_v,omega_v,config)

%% initializations
u_virt = zeros(6,1);

I_v = config.quad_ori_cntrl.params.uav_mass;
g = 9.81;

KR = config.cntrl_PDPlus.gains.KR;
Kw = config.cntrl_PDPlus.gains.Kw;

%% Get the desired orientation 

z_b_des = fd/norm(fd);
x_bp_des = Rz(yaw_des)*[1;0;0];
y_b_des = cross(z_b_des,x_bp_des)/norm(cross(z_b_des,x_bp_des));
x_b_des  = cross(y_b_des,z_b_des);
R_v_ref = [x_b_des, y_b_des, z_b_des];

R_dot = (R_v_ref - R_v_ref_t_1)./config.quad_ori_cntrl.params.dt;
omega_v_ref = R_v_ref' * R_dot;  % Desired Skew matrix
omega_v_ref = [omega_v_ref(3,2); -omega_v_ref(3,1); omega_v_ref(2,1) ]; % desired skew vector

eR = vmap(R_v_ref,R_v);
ew = omega_v_ref - omega_v;

%% compute u_virt

m_cmnd = KR*eR + Kw*ew  + ...
cross(omega_v,I_v*omega_v) - ...
I_v*(skewMat(omega_v)*R_v'*R_v_ref*omega_v_ref);


end