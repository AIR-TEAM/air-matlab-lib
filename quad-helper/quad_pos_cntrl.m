[f_des,f_cmnd] = quad_pos_cntrl(p_v_ref,p_v_dt_ref,p_v_ddt_ref,p_v,p_v_dt,R_v,config)

    m_v = config.quad_pos_cntrl.params.uav_mass;
    g = 9.81;

    Kp = config.quad_pos_cntrl.gains.Kp;
    Kv = config.quad_pos_cntrl.gains.Kv;

    ep =  p_v_ref - p_v;
    ev = p_v_dt_ref - p_v_dt;

    f_des = m_v*p_v_ddt_ref + Kv*ev + Kp*ep + m_v*g*[0;0;1]; 

    f_cmnd = f_des'*(R_v*[0;0;1]);

end
