function prop_speed_sq = quad_input_alloc(f_cmnd,m_cmnd,config)

    G = config.quad_cntrl.params.uav_wrenchMap;

    prop_speed_sq = G\[f_cmnd;m_cmnd];

end