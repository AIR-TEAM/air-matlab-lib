%% Initialize GenoM and connect the ports
%
%  Don't forget before to start the following console commands: 
%
%    h2 init, rotorcarft-genom3, optitrack-genom3, pom-genom3 genomixd
%
%% init Genomix
%% user specific parameters
username='amr';
exp_type = 1; %1 = sim , 2 = real test
genom_machine = 1; %genom3 components running: 1 = locally, 2 = onboard  
uav_code = 1; %1 = FThexPlus, 2 = FThex2

% In case of a simulatio, no need to change the params below
FC = 1 ; % flight controller  = (1 = paparazzi) , (2 = MK) 
exp_env = 1; % 1 = smart xp , 2 = RaM flight lab

%todo: add a calib_file of choice 
% calib_filename

%% Paths needed for the initialization 

disp('Initializing system:');

% Put the custom path that you used for the ROBOTPKG_BASE on your localhost
ROBOTPKG_BASE = strcat('/home/', username, '/openrobots/');
DEVEL_BASE = strcat('/home/', username, '/devel/');
% add genomix-matlab path
addpath(strcat(ROBOTPKG_BASE,'lib/matlab'));
addpath(strcat(ROBOTPKG_BASE,'lib/matlab/simulink'));
addpath(strcat(ROBOTPKG_BASE,'lib/matlab/simulink/genomix'));


%% Logging of data

% add log scripts path
%addpath('log_scripts/');

%% local machine paths
or_path = strcat(ROBOTPKG_BASE, 'lib/genom/pocolibs/plugins/'); 
devel_path = strcat(DEVEL_BASE, 'lib/genom/pocolibs/plugins/');

%% odroid paths
%or_path = strcat('/home/','odroid','/openrobots/lib/genom/pocolibs/plugins/'); 
%devel_path = strcat('/home/odroid/devel/lib/genom/pocolibs/plugins/');

%% Initialize genomix Client
if exp_type==2
    if uav_code == 1
        if genom_machine == 1
            host = 'localhost';
        elseif genom_machine == 2
            host = '192.168.0.105'; %FiberTHex plus
        else
           error('Invalid genom_machine only 1 or 2 are valid, Code execution has been stopped!');
        end
    elseif uav_code == 2
        if genom_machine == 1
            host = 'localhost';
        elseif genom_machine == 2
            host = '192.168.0.181'; %FiberTHex   
        else
           error('Invalid genom_machine only 1 or 2 are valid, Code execution has been stopped!');
        end
    else
        error('Invalid uav_code only 1 or 2 are valid, Code execution has been stopped!');
    end
elseif exp_type==1
        host = 'localhost';
else
        error('Invalid exp_type only 1 or 2 are valid, Code execution has been stopped!');
end
client = genomix.client(host);
%% init Rotorcraft
if exp_type==2
    if FC == 1
        usb_port = 'chimera-18';
        %%small flight lab
        %calib_filename = '2022_06_23_ut_fbthex_stddev_increased.mat';
        %%smart xp
        calib_filename = 'paparazzi_zero_set_smartxp_calib.mat';
        %calib_filename = '2022_06_23_ut_fbthex.mat';    
    elseif FC == 2
        usb_port = '/dev/ttyUSB0';
        calib_filename = 'may23_22_std_fixed.mat';    
    else
        error('Invalid FC only 1 or 2 are valid, Code execution has been stopped!');
    end
elseif exp_type==1
        usb_port = '/tmp/fiberthex';
        calib_filename = 'may23_22_std_fixed.mat';
else
        error('Invalid exp_type only 1 or 2 are valid, Code execution has been stopped!');
end

disp('---ROTORCRAFT---')
rotorcraft = client.load(strcat(or_path,'rotorcraft'));
pause(0.5);
result_rc = rotorcraft.connect(usb_port, 500000);
string    = ['Connecting to rotorcraft: ', result_rc.status];
disp(string);
pause(0.5)
s_rate_imu = 1000;
s_rate_mag = 1;
s_rate_motor = 16;
s_rate_battery = 1;
rotorcraft.set_sensor_rate(s_rate_imu,s_rate_mag,s_rate_motor,s_rate_battery);

if exp_type == 2
    if (exp_env == 2)
    load(calib_filename);
    elseif (exp_env == 1)
    load(calib_filename);
    end
    calibration = eval('calibration');
    calibration.imu_calibration.mscale= {0,0,0,0,0,0,0,0,0};
    cal_res = rotorcraft.set_imu_calibration(calibration);
    pause(1);
    pause(0.5)
    rotorcraft.set_imu_filter({20 20 20},{5 5 5},{0 0 0})
end

%% init optitrack
disp('---OPTITRACK---')
if exp_type == 1
    % Gazebo
    optitrack_ip = 'localhost';
    optitrack_cmd_port = '11345';
    optitrack_data_port = '1511';
    optitrack_multicast_adr = '239.192.168.30';
elseif exp_type == 2
    switch exp_env
    case 1
        % smart-xp
        optitrack_ip = '192.168.0.121';
        optitrack_cmd_port = '1510';
        optitrack_data_port = '1511';
        optitrack_multicast_adr = '239.255.42.99';
    case 2
        % RaM flight lab
        optitrack_ip = '192.168.0.102';
        optitrack_cmd_port = '1510';
        optitrack_data_port = '1511';
        optitrack_multicast_adr = '239.255.42.99';
    end
else
    error('Invalid exp_type only 1 or 2 are valid, Code execution has been stopped!');
end
 opti   = client.load(strcat(or_path,'optitrack'));
 pause(1);
 result = opti.connect(optitrack_ip, ...
                       optitrack_cmd_port, ...
                       optitrack_multicast_adr, ...
                       optitrack_data_port);  
stringmsg = ['Connecting to MoCap: ',result.status];
disp(stringmsg); 
 %% POM  
pom = client.load(strcat(or_path,'pom'));
pause(1);
% IMU
result = pom.connect_port('measure/imu', 'rotorcraft/imu'); 
string = ['Initializing POM connection to IMU: ',result.status];
pom.add_measurement('imu', 0,0,0,0,0,0);
disp(string);
% opti-track
result = pom.connect_port('measure/mocap', ...
                                       'optitrack/bodies/fiberthex');
            stringmsg = ['Initializing POM connection to optitrack: ', ...
                          result.status];
pom.add_measurement('mocap',0,0,0,0,0,0);
disp(stringmsg);
pom.set_history_length(0.3);
 %% setup dynamixel
% serial_port = '/dev/ttyACM0'; % (default: '/dev/ttyACM0')
% baud_rate = 1000000; % usb baud rate (dafault: 1000000)
% % --------- Load Dynamixel module
% fprintf('* Load Dynamixel module...');
% 
% dynamixel = client.load(strcat(devel_path,'dynamixel'));
% pause(1);
% fprintf('done\n');
% 
% % --------- Connect to Dynamixel module
% msg_conn_dynamixel = dynamixel.connect(serial_port, baud_rate);
% result_conn_dynamixel = msg_conn_dynamixel.status;
% dynamixel.set_velocity({0 0 0 0 0 0 0 0});
% 
% 
% fprintf('* Connect to Dynamixel: %s \n', result_conn_dynamixel);
% if strcmpi (result_conn_dynamixel, 'error') == 1
%     disp('>> Error during connection!');
%     disp(['> Exception: ', msg_conn_dynamixel.exception.ex]);
%     disp(['> Code: ', num2str(msg_conn_dynamixel.exception.detail.code)]);
%     disp(['> What: ', msg_conn_dynamixel.exception.detail.what]);
%     error('Code execution has been stopped!');
% end 
%% Joystick
joy = client.load(strcat(or_path,'joystick'));
string = 'Connecting to joystick: done';
%% BATTERY
batteryLevel = rotorcraft.get_battery();
disp(strcat('Battery level: ', num2str(batteryLevel.result.battery.level)));