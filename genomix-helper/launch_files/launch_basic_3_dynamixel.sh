#!/bin/bash

# Launch all required modules. Shouldn't matter if some of them (e.g. opti and pom) are
# not used in simulation or for the 3d_arm control. In this way, we avoid useless duplications 
h2 init

sleep 5

genomixd &
sleep 1
optitrack-pocolibs -f &
#sleep 1
pom-pocolibs -f  &
sleep 1
rotorcraft-pocolibs -f &
sleep 1
joystick-pocolibs -f &
#sleep 1
#$DEVEL_PATH/bin/dynamixel-pocolibs -f 
#sleep 1
#genomixd -v &
sleep 1
dynamixel-pocolibs -f --name dyn_brick1 &
dynamixel-pocolibs -f --name dyn_brick2 &
dynamixel-pocolibs -f --name dyn_brick3 
#sleep 1
#nhfc-pocolibs -f &
#sleep 1
#maneuver-pocolibs -f &
