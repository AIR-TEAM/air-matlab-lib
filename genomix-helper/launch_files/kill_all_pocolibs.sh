#!/bin/bash

h2 end &

killall optitrack-pocolibs
killall pom-pocolibs
killall rotorcraft-pocolibs
killall joystick-pocolibs
killall genomixd
killall dynamixel-pocolibs
killall mrsim-pocolibs
killall nhfc-pocolibs

rm ~/.*.pid-`hostname`
