function [p1_o_dt,w_o] = sim_object(R_v,omega_v,v_v,p1_o,R_o,config)
    
    Jac_o = [eye(3) skewMat(R_v*config.sim.params.payload_r)];
    p1_o_dt = Jac_o*[v_v;R_v*omega_v]; %I am assuming omega_v is in body frame
    w_o = R_v*omega_v;

        
    end