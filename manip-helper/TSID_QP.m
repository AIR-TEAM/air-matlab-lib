function [Hessian, gradient, upperBounds, lowerBounds,  EqualityConstrMatrix, ...
    biasVectorEqConstr, InequalityConstrMatrix, ubiasVectorIneqConstr, lbiasVectorIneqConstr] = ...
       TSID_QP(nu_e_star,nu_dot_star,PI,M,h,J,J_dot_nu,R_v,s,s_dot,measured_thrust,config)

       %% params
       JOINT_SPACE_CONTROL = config.TSID_QP.params.JOINT_SPACE_CONTROL;
       ndof = config.TSID_QP.params.ndof;
       nu = config.TSID_QP.params.nu;
       coef_f = config.TSID_QP.params.coef_f;
       joint_limits = config.TSID_QP.params.joint_limits;
       max_prop_thrust = config.TSID_QP.params.max_prop_thrust;
       min_prop_thrust = config.TSID_QP.params.min_prop_thrust;
       torque_limits = config.TSID_QP.params.torque_limits;
       W_config = config.TSID_QP.params.W_config;
       W_secondary_tasks = inv(M);
       W_thrust = config.TSID_QP.params.W_thrust;
       W_u = [diag(W_thrust*ones(6,1)) zeros(6,3);...
       zeros(3,6) diag(config.TSID_QP.params.W_tau_arm*ones(3,1))];
       dt = config.TSID_QP.params.dt;
       thrust_dt_lim = config.TSID_QP.params.thrust_dt_lim;
       W_slack = 0;
       %Jwxy = J(:,4:5);
       J_tilt = [0 0 0 1 0 0 0 0 0;0 0 0 0 1 0 0 0 0];
       Jwxy = J*pinv(J_tilt);
       P_tilt = (eye(9) - pinv(J_tilt)*J_tilt);

       %% regularization 
       thrustSymMatrix = [1 -1  0  0  0  0
                          0  1 -1  0  0  0
                          0  0  1 -1  0  0
                          0  0  0  1 -1  0
                          0  0  0  0  1 -1
                          0  0  0  0  0  0];

       %% QP formulation
       G_v =  [R_v zeros(3);...
               zeros(3) R_v]*config.TSID_QP.params.uav_wrenchMap;
        
       G_manip = [G_v zeros(6,3);...
                 zeros(3,6) eye(3)];  


       %Hessian_mat  = [W_config*W_secondary_tasks zeros(9,9) zeros(9,1);...
       %               zeros(6,9) W_thrust*PI zeros(6,3) zeros(6,1);...
       %               zeros(3,9) zeros(3,6) zeros(3,3) zeros(3,1);...
       %               zeros(1,9) zeros(1,6) zeros(1,3) W_slack];

       Hessian_mat  = [W_config*W_secondary_tasks zeros(9,9) zeros(9,1);...
                      zeros(9,9) W_u zeros(9,1);...
                      zeros(1,9) zeros(1,9) W_slack];      

       %if JOINT_SPACE_CONTROL
       %    flag = 0;
       %    temp_weight = [W_secondary_tasks(1:6,1:6) zeros(6,3);...
       %                     zeros(3,9)]; 
       %    Hessian_mat  = [temp_weight zeros(nu);zeros(ndof) zeros(nu)];
       %end 
       
       Hessian  = 0.5*(transpose(Hessian_mat) + Hessian_mat);

       %f_star = coef_f*80^2
       gradient = ([-nu_dot_star' zeros(9,1)' 0]*Hessian_mat)';
     
       TILT_PRIO = 1;
       if TILT_PRIO
           EqualityConstrMatrix = [M -G_manip zeros(9,1);J*P_tilt zeros(6,9) -nu_e_star;...
                [0 0 0 1 0 0 0 0 0;0 0 0 0 1 0 0 0 0] zeros(2,9) zeros(2,1)];
           biasVectorEqConstr = [-h;nu_e_star-J_dot_nu-Jwxy*nu_dot_star(4:5);...
                nu_dot_star(4:5)];
       else
            %EqualityConstrMatrix = [M -G_manip;J zeros(6,9)];
            %biasVectorEqConstr = [-h;nu_e_star-J_dot_nu];

            EqualityConstrMatrix = [M -G_manip zeros(9,1);J zeros(6,9) -nu_e_star];...
           biasVectorEqConstr = [-h;nu_e_star-J_dot_nu];
       end
       

       thrust_dt_max = measured_thrust + ones(6,1)*thrust_dt_lim*dt;
       thrust_dt_min = measured_thrust + (-ones(6,1)*thrust_dt_lim)*dt;

       thrust_max =  zeros(6,1);
       thrust_min =  zeros(6,1);

       for i=1:6
        max_i = min(thrust_dt_max(i),max_prop_thrust);
        min_i = max(thrust_dt_min(i),min_prop_thrust);
        thrust_max(i) = max_prop_thrust;
        thrust_min(i) = min_prop_thrust;
       end

       s_ddot_ub = (joint_limits - s - s_dot*dt)./dt;
       s_ddot_lb = (-joint_limits - s - s_dot*dt)./dt;

       upperBounds = [ones(6,1)*inf;...
       s_ddot_ub;...
       thrust_max;...
       torque_limits*ones(3,1);0];
       
       lowerBounds = [ones(6,1)*-inf;...
       s_ddot_lb;...
       thrust_min;...
       -torque_limits*ones(3,1);0];

       %epsilon = 2;

       InequalityConstrMatrix = EqualityConstrMatrix;
       ubiasVectorIneqConstr = biasVectorEqConstr;
       lbiasVectorIneqConstr = biasVectorEqConstr;

end
