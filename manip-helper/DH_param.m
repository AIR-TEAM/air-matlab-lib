function i_1Ti =  DH_param(a,alpha,d,q)
  cq = cos(q);
  sq = sin(q);
  calpha = cos(alpha);
  salpha = sin(alpha);
  
i_1Ti = [cq -calpha*q salpha*sq a*cq;...
        sq calpha*cq -salpha*cq a*sq;...
        0 salpha calpha d;...
        0 0 0 1];


end