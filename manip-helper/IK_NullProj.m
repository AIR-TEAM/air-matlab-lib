function u_q = IK_NullProj(u_e,J_ee,Jdt_nu,q_dt,W,H,config)
    %IK_NullProj: Is a general weighted pinv inverse kinematics @ acceleration level 
    % with secondary configuration level tasks
    q_star = zeros(9,1);
    for idx=1:config.manip.ndof
        q_star(idx) = ManipConfigTask(H(idx),q_dt(idx),idx,config);
    end
    J_wpinv  =  W * J_ee' * inv( J_ee * W * J_ee' );
    P = (eye(config.manip.ndof) - J_wpinv*J_ee);
    u_q = J_wpinv*(u_e - Jdt_nu) + P*W*q_star;
end
    
    