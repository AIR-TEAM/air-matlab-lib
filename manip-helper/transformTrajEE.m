function [v_p_ee_d,v_v_ee_d] = transformTrajEE(p_v,R_v,p_e)

    %p_e = p_v + R_v*v_p_e;
    v_p_ee_d = R_v'*(p_e - p_v);
    v_v_ee_d = zeros(3,1);

    %v_ee_d = v_v + R_dot*v_p_e + R_v*v_v_e;

end