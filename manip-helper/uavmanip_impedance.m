function [Hessian, gradient, upperBounds, lowerBounds,  EqualityConstrMatrix, ...
    biasVectorEqConstr, InequalityConstrMatrix, ubiasVectorIneqConstr, lbiasVectorIneqConstr] = ...
       uavmanip_impedance(aref,M,h,R_v,h_x,L_hx,L_hu,config)


%% Problem formulation  
% nu_dt_star = null space damping
%    min    ||nu_dt - nu_dt_star||^2_M + || ew0 + cd*u*w_meas*dt ||^2
%  nu_dt,u
%  M*nu_dt + h = Gu + J'W
%  limits on u and nu_dt
%% initialization
weights_task1 = diag(config.cntrl_QP.weights.uav_translation_task);
weights_task2 = diag(config.cntrl_QP.weights.uav_orientation_task);
umax = config.sat.maxThrust;
umin = config.sat.minThrust;
alpha = config.cbf_QP.alpha;

if config.cntrl_QP.intrinsic == 1
  R_G_v = [R_v zeros(3);zeros(3) eye(3)];
  G_v = R_G_v*config.cntrl_QP.params.uav_wrenchMap;
else
  R_G_v = [R_v zeros(3);zeros(3) R_v];
  G_v = R_G_v*config.cntrl_QP.params.uav_wrenchMap;
end

W = [weights_task1 zeros(3,3); ...
  zeros(3,3) weights_task2];

I = M\G_v;
b = -(M\h) - aref;

W_prime = I'*W*I;

%% QP variables definition


% Hessian matrix
Hessian  = W_prime;

% enforce Hessian matrix symmetry
Hessian  = 0.5*(transpose(Hessian) + Hessian);

% gradient       
gradient = (b'*W*I)';
    
% upper and lower bounds
upperBounds = umax*ones(6,1);
lowerBounds = umin*ones(6,1);

% equality constraint

EqualityConstrMatrix = zeros(1,6);
biasVectorEqConstr = 0;


InequalityConstrMatrix = L_hu;

lbiasVectorIneqConstr = -alpha*h_x - L_hx;
                   
ubiasVectorIneqConstr = 1e10;




end