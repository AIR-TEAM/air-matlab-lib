function tau = uavmanip_cntrl_FL(u_virt,M_am,h_am,FText,config)
    %% initializations
    
    Inertia_mat = M_am;
    bias_forces = h_am;
    
    %% feedback linearization
    
    tau  = Inertia_mat*u_virt + bias_forces - FText;
    
end