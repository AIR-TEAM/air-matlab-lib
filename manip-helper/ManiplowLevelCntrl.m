function tau_m = ManiplowLevelCntrl(u_virt,M_m,h_m,FText,config)
    %% initializations
    
    Inertia_mat = M_m;
    bias_forces = h_m;
    
    %% feedback linearization
    
    tau_m  = Inertia_mat*u_virt + bias_forces - FText;
    
    end