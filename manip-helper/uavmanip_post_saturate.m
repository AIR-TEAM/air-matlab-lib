function u = uavmanip_post_saturate(u,tau_m,config)
    for i=1:6
        if ~(u(i)<= config.sat.maxThrust && u(i)>= config.sat.minThrust)
               if (u(i) > config.sat.maxThrust)
                   u(i) = config.sat.maxThrust;
               else
                   u(i) = config.sat.minThrust;
               end
        end
           
    end

    for i=1:3
        if ~(tau_m(i)<= config.sat.maxTau_m && tau_m(i)>= config.sat.minTau_m)
               if (tau_m(i) > config.sat.maxTau_m)
                    tau_m(i) = config.sat.maxTau_m;
               else
                    tau_m(i) = config.sat.minTau_m;
               end
        end
           
    end
   
end