function u_q = IK_TiltPrioNullProj(u_e,R_des,R_v,w_v,J_ee,Jdt_nu,q_dt,W,H,config)
    %IK_NullProj: Is a general weighted pinv inverse kinematics @ acceleration level 
    % with secondary configuration level tasks

    %% UAV tilt control task
    
    Kr = [70 0 0;0 70 0;0 0 70];
    Kw = [12 0 0;0 12 0;0 0 12];    

    [angle_xy,axis_xy,axis_z,error_xy,error_z] = S2error(R_des,R_v);
    nu_w_dot =  -Kr*R_v*[error_xy(1:2);0] - Kw*(w_v);

    %% UAV tilt control task

    q_star = zeros(9,1);
    for idx=1:config.manip.ndof
        q_star(idx) = ManipConfigTask(H(idx),q_dt(idx),idx,config);
    end

    %% Augmented Redundant Tasks 
    J_c = [J_ee(:,1:3) J_ee(:,6:9)];
    Jwxy = J_ee(:,4:5);
    J_wpinv  =  W * J_c' * inv( J_c * W * J_c' );
    P = (eye(config.manip.ndof-2) - J_wpinv*J_c);
    u_q_mv = J_wpinv*(u_e - Jdt_nu - Jwxy*nu_w_dot(1:2)) + P*W*([q_star(1:3);q_star(6);q_star(7:9)]);
    u_q = [u_q_mv(1:3);nu_w_dot(1:2);u_q_mv(4:7)];
end
    





