function [p_v_ddt,w_v_dt,q_m_ddt] = sim_fthexManip_decoupled(prop_speed,v_m_des,q_m_0,v_m_prev,...
                                                            M_vm,h_vm,R_v,FText,residual_W,config)
    %% General simulation for the fthex that considers the presence of residual 
    %%dynamics and external forces
    
    dt = config.sim.params.dt;

    %% initializations

    Inertia_mat = M_vm;
    bias_forces = h_vm;

    %% Manipulator integrator dynamics 
    q_m = q_m_0  + v_m_des*dt;
    q_m_ddt = (v_m_des - v_m_prev)/dt;

    G_v = config.sim.params.uav_wrenchMap;
    R_G_v = [R_v zeros(3);zeros(3) R_v];

    w_G_v = R_G_v*G_v;
    
    %% Control wrench computation
    cntrl_W = w_G_v*prop_speed;

    %% forward dynamics 
    uav_Inertia_mat = Inertia_mat(1:6,1:6);
    uav_dyn_coup = Inertia_mat(1:6,7:9)*q_m_ddt;
    acc_vm = uav_Inertia_mat\(-bias_forces(1:6)-uav_dyn_coup-residual_W(1:6)+cntrl_W(1:6)+FText(1:6));

    p_v_ddt = acc_vm(1:3);
    w_v_dt = acc_vm(4:6);
    %q_m_ddt = acc_vm(7:9);



    end