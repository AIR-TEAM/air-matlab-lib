function [p_v_ddt,w_v_dt,q_m_ddt] = sim_fthexManip(prop_speed,tau_m,M_vm,h_vm,R_v,FText,residual_W,config)
    %% General simulation for the fthex that considers the presence of residual 
    %%dynamics and external forces
    
    %% initializations

    Inertia_mat = M_vm;
    bias_forces = h_vm;

    G_v = config.sim.params.uav_wrenchMap;
    R_G_v = [R_v zeros(3);zeros(3) R_v];

    w_G_v = R_G_v*G_v;

    ipMap = [w_G_v zeros(6,3);...
            zeros(3,6) eye(3)];
    
    %% Control wrench computation
    cntrl_W = ipMap*[prop_speed;tau_m];

    %% forward dynamics 
    acc_vm = Inertia_mat\(-bias_forces-residual_W+cntrl_W+FText);
    
    p_v_ddt = acc_vm(1:3);
    w_v_dt = acc_vm(4:6);
    q_m_ddt = acc_vm(7:9);

    end