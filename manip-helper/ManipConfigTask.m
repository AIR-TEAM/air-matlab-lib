function q_star = ManipConfigTask(H,q_dt,idx,config)
    Kp = config.manip.configTask.gain.Kp(idx);
    Kd = config.manip.configTask.gain.Kd(idx);

    q_star = Kp*H - Kd*q_dt;
end
