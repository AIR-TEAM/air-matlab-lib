function v_virt = Pplus(p_ee_d,v_ee_d,p_ee,J_q,q_a,q_des,TAKEOFF,config)


    Kp = config.cntrl_Pplus.gains.Kp;
    Kp_q = config.cntrl_Pplus.homing.gains.Kp;   
    ep = p_ee_d - p_ee;

    % implement some saturation

    % implement some singularity handling
    %if cond(J_q) < threshold
    %    v_virt = zeros(3,1);
    %else
    %    v_virt = J_q\(v_ee_d + Kp*ep);
    %end
    if TAKEOFF == 1
        v_virt = J_q(1:3,:)\(v_ee_d + Kp*ep);
    else
        v_virt =  Kp_q*(q_des - q_a);
        %v_virt = zeros(3,1);
    end

end