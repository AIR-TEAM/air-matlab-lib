function [Hessian, gradient, upperBounds, lowerBounds,  EqualityConstrMatrix, ...
    biasVectorEqConstr, InequalityConstrMatrix, ubiasVectorIneqConstr, lbiasVectorIneqConstr] = ...
       TiltPrio_TSID_QP(nu_e_star,nu_dot_star,f_thrust_star,M,h,J,J_dot_nu,R_v,s,s_dot,w_v,config)

       %% params
       JOINT_SPACE_CONTROL = config.TSID_QP.params.JOINT_SPACE_CONTROL;
       ndof = config.TSID_QP.params.ndof;
       nu = config.TSID_QP.params.nu;
       coef_f = config.TSID_QP.params.coef_f;
       joint_limits = config.TSID_QP.params.joint_limits;
       max_prop_thrust = config.TSID_QP.params.max_prop_thrust;
       min_prop_thrust = config.TSID_QP.params.min_prop_thrust;
       torque_limits = config.TSID_QP.params.torque_limits;
       W_config = config.TSID_QP.params.W_config;
       
       M_manip = [M(1:3,1:3) zeros(3,2) M(1:3,6:9);...
                  M(6:9,1:3) zeros(4,2) M(6:9,6:9)];
       
       W_secondary_tasks = inv(M);

       W_thrust = diag(config.TSID_QP.params.W_thrust*ones(6,1));
       dt = config.TSID_QP.params.dt;

       %% regularization 
       Jwxy = J(:,4:5);
    
        Kr = [70 0 0;0 70 0;0 0 70];
        Kw = [12 0 0;0 12 0;0 0 12];    

        [angle_xy,axis_xy,axis_z,error_xy,error_z] = S2error(eye(3),R_v);
        nu_w_dot =  -Kr*R_v*[error_xy(1:2);0] - Kw*(w_v);

       %% QP formulation
       G_v =  [R_v zeros(3);...
               zeros(3) R_v]*config.TSID_QP.params.uav_wrenchMap;
        
       G_manip = [G_v zeros(6,3);...
                 zeros(3,6) eye(3)];  


       Hessian_mat  = [W_config*W_secondary_tasks zeros(9,9);...
                      zeros(6,9) zeros(6,6) zeros(6,3);...
                      zeros(3,9) zeros(3,6) zeros(3,3)];
       %if JOINT_SPACE_CONTROL
       %    flag = 0;
       %    temp_weight = [W_secondary_tasks(1:6,1:6) zeros(6,3);...
       %                     zeros(3,9)]; 
       %    Hessian_mat  = [temp_weight zeros(nu);zeros(ndof) zeros(nu)];
       %end 
       
       Hessian  = 0.5*(transpose(Hessian_mat) + Hessian_mat);


       f_star = coef_f*80^2
       gradient = ([-nu_dot_star' zeros(9,1)']*[W_secondary_tasks zeros(9);zeros(6,9) zeros(6,6) zeros(6,3);...
       zeros(3,9) zeros(3,6) zeros(3,3)])';

       
       EqualityConstrMatrix = [M -G_manip;J zeros(6,9);[0 0 0 1 0 0 0 0 0;0 0 0 0 1 0 0 0 0] zeros(2,9)];
       biasVectorEqConstr = [-h;nu_e_star-J_dot_nu-Jwxy*nu_w_dot(1:2);nu_w_dot(1:2)];

       %if JOINT_SPACE_CONTROL
       %    flag = 0;
       %    EqualityConstrMatrix = [M -G_manip;flag*J zeros(6,9)];
       %    biasVectorEqConstr = [-h;flag*nu_e_star-flag*J_dot_nu];
       %end 

       s_ddot_ub = (joint_limits - s - s_dot*dt)./dt;
       s_ddot_lb = (-joint_limits - s - s_dot*dt)./dt;

       upperBounds = [ones(6,1)*inf;...
       s_ddot_ub;...
       max_prop_thrust*ones(6,1);...
       torque_limits*ones(3,1)];
       
       lowerBounds = [ones(6,1)*-inf;...
       s_ddot_lb;...
       min_prop_thrust*ones(6,1);...
       -torque_limits*ones(3,1)];

       epsilon = 2;

       InequalityConstrMatrix = EqualityConstrMatrix;
       ubiasVectorIneqConstr = biasVectorEqConstr;
       lbiasVectorIneqConstr = biasVectorEqConstr;

end
