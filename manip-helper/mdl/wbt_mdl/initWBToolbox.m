%% %%%%% WBToolbox parameters %%%%

%% Robot configuration for WBToolbox
WBTConfigRobot           = WBToolbox.Configuration;
WBTConfigRobot.RobotName = 'fiberthex_3dof_arm';
WBTConfigRobot.UrdfFile  = 'f_tilthex_plus.urdf';
%WBTConfigRobot.UrdfFile  = 'model_wbt.urdf';
WBTConfigRobot.LocalName = 'WBT';

% Controlboards and joints list. Each joint is associated to the corresponding controlboard 
WBTConfigRobot.ControlBoardsNames = {'three_dof_arm_joints'};
WBTConfigRobot.ControlledJoints   = [];

ControlBoards                                        = struct();
ControlBoards.(WBTConfigRobot.ControlBoardsNames{1}) = {'joint1','joint2','joint3'};
% ControlBoards.(WBTConfigRobot.ControlBoardsNames{1}) = {'joint1','joint2'};


for n = 1:length(WBTConfigRobot.ControlBoardsNames)

    WBTConfigRobot.ControlledJoints = [WBTConfigRobot.ControlledJoints, ...
                                       ControlBoards.(WBTConfigRobot.ControlBoardsNames{n})];
end

% Frames list
Frames.PROP_1          = 'propellor1';
Frames.PROP_2          = 'propellor2';
Frames.PROP_3          = 'propellor3';
Frames.PROP_4          = 'propellor4';
Frames.PROP_5          = 'propellor5';
Frames.PROP_6          = 'propellor6';
Frames.FRAME_END_EFF   = '3d_arm_end_effector';
Frames.FRAME_FT_BOTTOM = '3d_arm_base';

% Ports list
Ports.BASE_STATE_PORT = '/fiberthex_3dof_arm/floating_base/state:o';
Port.FT_UP_PORT       = '/fiberthex_3dof_arm/top_ft/analog:o';
Port.FT_BOTTOM_PORT   = '/fiberthex_3dof_arm/bottom_ft/analog:o';
