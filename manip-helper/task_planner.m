function reference = task_planner(config)

    no_waypoints = size(config.plan.params.wp,2);
    wp_duration = config.plan.params.wp_duration;
    dt = config.time.plan.dt;

    % In reactive fastion define cost functions that lead to 
    % minimizing power consumption tau_v
    % given current configuration we can compute tau wrt to 
    % config and velocity, we can compute the gradient wrt to configurtions 
    % and use it for arm positioning, I want a cost function that is very high 
    % if we get close the limits but equal more or less everywhere else 
    % we can take into account acceleration limits, based on Davide's cost
    % obstacle avoidance constraints and penalizations on the distance. 
    % fiberthex, we have as poi props, differential gear, done 3 times. 
    % Imagine, you simulate the low level control tracking of a trajectory
    % and then you refine it with the NMPC-Sensitivity planner thing. 
    
    for i=1:no_waypoints
        x0;
        xf;

        v0;
        vf;

        a0;
        af;

        for t=0:wp_duration(wp_idx)
            % call polynomial functions
        end
    end




end